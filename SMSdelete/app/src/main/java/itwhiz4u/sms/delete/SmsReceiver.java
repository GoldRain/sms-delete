package itwhiz4u.sms.delete;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by ITWhiz4U on 11/1/2017.
 */

public class SmsReceiver extends BroadcastReceiver {
    /*@Override
    public void onReceive(Context context, Intent intent) {

        Bundle bundle = intent.getExtras();

        if (bundle == null){

            Log.d("aaaaaaaaaaaaaaaa", "null======>");
        }
        SmsMessage[] msgs = null;
        String str = "";

        Toast.makeText(context, "aaaaaaaaaaaaaaaaaa", Toast.LENGTH_SHORT).show();

        if (bundle != null)
        {
            //---retrieve the SMS message received--
            Object[] pdus = (Object[]) bundle.get("pdus");

            msgs = new SmsMessage[pdus.length];

            for (int i = 0; i < msgs.length; i++){
                msgs[i]  = SmsMessage.createFromPdu((byte[])pdus[i]);

                String phNum = msgs[i].getOriginatingAddress();

                str += msgs[i].getMessageBody().toString();

                Toast.makeText(context, str, Toast.LENGTH_SHORT).show();

                if (phNum != null)
                {

                    Log.d("SMS========>", phNum);
                    Uri uri = Uri.parse("content://sms/inbox");

                    ContentResolver contentResolver = context.getContentResolver();

                    String where = "address=" + phNum;

                    Cursor cursor = contentResolver.query(uri, new String[] {"_id", "thread_id"}, where, null, null);

                    while (cursor.moveToNext()){

                        long thread_id = cursor.getLong(1);
                        where = "thread_id=" + thread_id;
                        Uri thread = Uri.parse("content://sms/inbox");
                        context.getContentResolver().delete(thread, where, null);
                    }
                } else Log.d("Null====>","aaaaaaaaaaa");
            }
        }
    }*/

    @Override
    public void onReceive(Context context, Intent intent) {
        //final String tag = TAG + ".onReceive";
        Bundle bundle = intent.getExtras();
        if (bundle == null) {
            //Log.w(tag, "BroadcastReceiver failed, no intent data to process.");

            Toast.makeText(context, "get Message", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!intent.getAction().equals(/*SMS_RECEIVED*/"")) {
            //Log.d(tag, "SMS_RECEIVED");

            String smsOriginatingAddress, smsDisplayMessage;

            // You have to CHOOSE which code snippet to use NEW (KitKat+), or legacy
            // Please comment out the for{} you don't want to use.

            // API level 19 (KitKat 4.4) getMessagesFromIntent
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                for (SmsMessage message : Telephony.Sms.Intents.
                        getMessagesFromIntent(intent)) {
                    //Log.d(tag, "KitKat or newer");
                    if (message == null) {
                        //Log.e(tag, "SMS message is null -- ABORT");
                        break;
                    }
                    smsOriginatingAddress = message.getDisplayOriginatingAddress();
                    //see getMessageBody();
                    smsDisplayMessage = message.getDisplayMessageBody();
                    processReceivedSms(smsOriginatingAddress, smsDisplayMessage);
                }
            }

            // Processing SMS messages the OLD way, before KitKat,
            // this WILL work on KitKat or newer Android
            // PDU is a “protocol data unit”, which is the industry
            // format for an SMS message
            Object[] data = (Object[]) bundle.get("pdus");
            for (Object pdu : data) {
                //Log.d(tag, "legacy SMS implementation (before KitKat)");
                SmsMessage message = SmsMessage.createFromPdu((byte[]) pdu);
                if (message == null) {
                    //Log.e(tag, "SMS message is null -- ABORT");
                    break;
                }
                smsOriginatingAddress = message.getDisplayOriginatingAddress();
                // see getMessageBody();
                smsDisplayMessage = message.getDisplayMessageBody();
                processReceivedSms(smsOriginatingAddress, smsDisplayMessage);
            }
        } // onReceive method

    }

    private void processReceivedSms(String aa, String bb){

        Log.d("aaaaaa++>", aa);

        Log.d("bbb====>", bb);

    }
}
